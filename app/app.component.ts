import { Component } from '@angular/core';
import {MemberService} from './member.service';

@Component({
  providers: [MemberService],
  selector: 'my-app',
  template: `
  <div>
  <button (click)="clearCache()">Clear Cache</button>
  <button (click)="reset()">Reset</button>
</div>

<table>
    <tr *ngIf="display.first">
        <td>    
            <h2>Members list</h2>
            <members-list></members-list>
            <button (click)="remove('first')">Remove</button>
        </td>
    </tr>
    <tr *ngIf="display.second">
        <td>    
            <h2>Members Cache</h2>
            <members-cache></members-cache>
            <button (click)="remove('second')">Remove</button>
        </td>
    </tr>
</table>
  `,
  
})
export class AppComponent  { 
  
  display = {first: true, second: true};

  
  constructor(private membersService: MemberService){}

  remove(list: string){
    this.display[list] = false;
  }

  reset(){
    this.display ={first: true, second: true};
  }

  clearCache(){
    this.membersService.clearCache();
  }
}
