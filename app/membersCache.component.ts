import {Component, OnInit, OnDestroy} from '@angular/core';
import {MemberService} from './member.service';
import {Member} from './member'


@Component({
     selector:"members-cache",
     template:`
     <div>
     <ul class="members">
     <li *ngFor="let member of members">
     <span class="badge">{{member.id}}</span><span>{{member.name}}</span>
     </li>
     </ul>
     </div>
   
     `
})

export class MembersCache implements OnInit{

    members = [Member];
    subscription : any;
    selectedMember : Member;
    MEMBERS = [Member];

constructor(private memberService:MemberService){}

loadData()
{
    this.subscription = this.memberService
                        .getMembers()
                        .subscribe(res => this.members = res,
                                    error => console.log(error));
                                    
}

ngOnInit(){
    this.loadData();
    this.MEMBERS = this.members;
}

ngOnDestroy(){
    this.subscription.unsubscribe();
    console.log('Destroyed');
}

}