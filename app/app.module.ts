import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import { FormsModule }   from '@angular/forms';

import {MembersList} from './members-list';
import { AppComponent }  from './app.component';
import {MemberDetail} from './memberDetail.component';
import {MembersCache} from './membersCache.component'

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule ],
  providers: [],
  declarations: [ AppComponent, MembersList, MemberDetail,MembersCache ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
