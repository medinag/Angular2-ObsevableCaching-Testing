import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'


@Injectable()

export class MemberService{

members: Observable<any> = null;

constructor(private http:Http){}

clearCache(){
    this.members= null;
}

getMembers(){
    if(!this.members){
        this.members = this.http.get('./app/members.json')
        .map((res:Response)=> res.json().members)
        .do(members=> console.log("fetched members"))
        .publishReplay(1,10000)
        .refCount();
    }
    return this.members;
}

}